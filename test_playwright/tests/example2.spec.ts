import { test, Page,expect } from '@playwright/test';

// Annotate entire file as serial.
test.describe.configure({ mode: 'serial' });

let page: Page;

test.beforeAll(async ({ browser }) => { page = await browser.newPage(); });
test.afterAll(async () => { await page.close(); });



test.describe("Spec2",async() =>{

test('example2 test1 @regression', async ({}, TestInfo) => {
  await page.goto('https://playwright.dev/');

  console.log("example2 test1: ",TestInfo.workerIndex);
});

test('example2 test2 @regression', async ({}, TestInfo) => {
  // await expect(page).toHaveTitle("Fast and reliable end-to-end testing for modern web apps | Playwright");
  await expect(page).toHaveTitle("Fast and reliable end-to-end testing for modern web apps | Playwright");
  console.log("example2 test2: ",TestInfo.workerIndex);
});

test('example2 test3',  async ({}, TestInfo) => {
  await page.getByText('Get Started').click();

  console.log("example2 test3: ",TestInfo.workerIndex);
});
})


test.describe("Spec2.2",async() =>{

  test('example2.2 test1 @regression', async ({}, TestInfo) => {
    await page.goto('https://playwright.dev/');
  
    console.log("example2.2 test1: ",TestInfo.workerIndex);
  });
  
  test('example2.2 test2 @regression', async ({}, TestInfo) => {
    await expect(page).toHaveTitle("Fast and reliable end-to-end testing for modern web apps | Playwright");
    console.log("example2.2 test2: ",TestInfo.workerIndex);
  });
  
  test('example2.2 test3',  async ({}, TestInfo) => {
    await page.getByText('Get Started').click();
  
    console.log("example2.2 test3: ",TestInfo.workerIndex);
  });
  })


  test.describe("Spec2.3",async() =>{

    test('example2.3 test1 @regression', async ({}, TestInfo) => {
      await page.goto('https://playwright.dev/');
    
      console.log("example2.3 test1: ",TestInfo.workerIndex);
    });
    
    test('example2.3 test2 @regression', async ({}, TestInfo) => {
      await expect(page).toHaveTitle("Fast and reliable end-to-end testing for modern web apps | Playwright");
      console.log("example2.3 test2: ",TestInfo.workerIndex);
    });
    
    test('example2.3 test3',  async ({}, TestInfo) => {
      await page.getByText('Get Started').click();
    
      console.log("example2.3 test3: ",TestInfo.workerIndex);
    });
    })