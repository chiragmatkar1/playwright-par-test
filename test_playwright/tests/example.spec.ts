import { test, expect , Page} from '@playwright/test';


let page: Page;

test.beforeAll(async ({ browser }) => { page = await browser.newPage(); });
test.afterAll(async () => { await page.close(); });

// Annotate entire file as serial.
test.describe.configure({ mode: 'serial' });


test.describe("Spec1",async() =>{


test('example1 test1 @regression', async ({}, TestInfo) => {
  await page.goto('https://playwright.dev/');

  // Expect a title "to contain" a substring.
  // await expect(page).toHaveTitle(/Playwright/);
  await expect(page).toHaveTitle(/Playwright/);

  console.log("example1 test1: ",TestInfo.workerIndex);

});

test('example1 test2 @regression', async ({}, TestInfo) => {
  //await page.goto('https://playwright.dev/');

  // Click the get started link.
  await page.getByRole('link', { name: 'Get started' }).click();

  // Expects the URL to contain intro.
  await expect(page).toHaveURL(/.*intro/);
  console.log("example1 test2",TestInfo.workerIndex);
});

})
